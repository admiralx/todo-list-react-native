import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

export default AddItem = ({currentItemText, handleCurrentInputItem, handleAddItem}) => (
    <View>
        <TextInput 
            placeholder="Add Item..." 
            style={styles.input} 
            onChangeText={(textVal) => handleCurrentInputItem(textVal)}
            value={currentItemText}
        />
        <TouchableOpacity style={styles.btn} onPress={handleAddItem}>
            <Text style={styles.btnText}>
                <Icon name="plus" size={20} /> Add Item</Text>
        </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({
    input: {
        height: 60,
        padding: 8,
        fontSize: 16
    },
    btn: {
        backgroundColor: '#c2bad8',
        padding: 9,
        margin: 5
    },
    btnText: {
        color: 'darkslateblue',
        fontSize: 20,
        textAlign: 'center'
    }
});
