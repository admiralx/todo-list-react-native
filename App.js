import React, { useState } from 'react';
import { View, Text, StyleSheet, Image, FlatList, Alert } from 'react-native';

import AddItem from './components/AddItem';
import Header from './components/Header';
import Item from './components/Item';

const uuid = () => {
    function s(n) { return h((Math.random() * (1<<(n<<2)))^Date.now()).slice(-n); }
    function h(n) { return (n|0).toString(16); }
    return  [
        s(4) + s(4), s(4),
        '4' + s(3),                    // UUID version 4
        h(8|(Math.random()*4)) + s(3), // {8|9|A|B}xxx
        // s(4) + s(4) + s(4),
        Date.now().toString(16).slice(-10) + s(2) // Use timestamp to avoid collisions
    ].join('-');
}

const App = () => {

    const [items, setItems] = useState([
        { id: uuid(), text: "Milk" },
        { id: uuid(), text: "Eggs" },
        { id: uuid(), text: "Bread" },
        { id: uuid(), text: "Jouice" }
    ]);
    const [currentItemText, setCurrentItemText] = useState('');

    const removeItem = (id) => {
        setItems(prev => {
            return prev.filter(item => item.id !== id);
        });
    }

    const handleCurrentInputItem = (textVal) => {
        setCurrentItemText(textVal);
    }

    const handleAddItem = () => {
        if (currentItemText.length < 1) {
            Alert.alert('Error', 'Please enter a valid item', {text: 'Ok'});
            return;
        }

        setItems(prev => [...prev, {id: uuid(), text: currentItemText}]);
        setCurrentItemText('');
    }

    return (
        <View style={styles.container}>
            <Header title="Shopping List" />
            <AddItem 
                currentItemText={currentItemText}
                handleCurrentInputItem={handleCurrentInputItem}
                handleAddItem={handleAddItem}
            />
            <FlatList
                data={items}
                renderItem={({ item }) =>
                    <Item item={item} handleDelete={removeItem}/>
                }
                keyExtractor={item => item.id}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 0,
    },
    img: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
    }
});

export default App;
